<?php
global $CONF;
$CONF = [
    'pathToArma3' => 'D:\Steam\steamapps\common\Arma 3',
    'pathToScreenshots' => 'D:\test\Users\prohi\Screenshots',
    'pathResultDir' => __DIR__.'/result',
    'paths' => [
        'Addons',
        'Argo\Addons',
        'Contact\Addons',
        'Curator\Addons',
        'Enoch\Addons',
        'Expansion\Addons',
        'Heli\Addons',
        'Jets\Addons',
        'Kart\Addons',
        'Mark\Addons',
        'Orange\Addons',
        'Tacops\Addons',
        'Tank\Addons',
        '!Workshop\@CBA_A3\addons',
        '!Workshop/@CUP Vehicles\addons',
        '!Workshop\@CUP Weapons\addons',
        '!Workshop\@ExileMod\addons',
        '!Workshop\@CUP Terrains - Core\addons',
        '!Workshop\@CUP Terrains - Maps 2.0\addons'
    ],
    'replacePaths' => [
        '^/CUP/Weapons/',
        '^CUP/Weapons/',
        '^/CUP/WheeledVehicles/',
        '^CUP/WheeledVehicles/',
        '^/CUP/WaterVehicles/',
        '^CUP/WaterVehicles/',

        '^/CUP/TrackedVehicles/',
        '^CUP/TrackedVehicles/',

        '^/CUP/airvehicles/',
        '^CUP/airvehicles/',

        '^A3/',
        '^/A3/',
        '^/'
    ]
];