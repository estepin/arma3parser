<?php


class Arma3ParserReport
{
    protected $convertAndSaveReport;

    /**
     * @return mixed
     */
    public function getConvertAndSaveReport() : ConvertSaveReport
    {
        return $this->convertAndSaveReport;
    }

    /**
     * @param mixed $convertAndSaveReport
     */
    public function setConvertAndSaveReport(ConvertSaveReport $convertAndSaveReport): void
    {
        $this->convertAndSaveReport = $convertAndSaveReport;
    }


}