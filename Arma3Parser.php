<?php
include_once "boot.php";

class Arma3Parser
{
    protected $resultSaver;
    protected $cfgCollector;
    protected $paths;

    function __construct(ResultSaver $resultSaver)
    {
        $this->cfgCollector = new CfgCollector();
        $this->resultSaver = $resultSaver;
    }


    function setPaths($paths)
    {
        $this->paths = $paths;
    }

    function run() : Arma3ParserReport
    {
        $armaObjects = $this->cfgCollector->parse($this->paths);
        $reportSave = $this->resultSaver->convertAndSave($armaObjects);

        $report = new Arma3ParserReport();
        $report->setConvertAndSaveReport($reportSave);

        return $report;
    }

}