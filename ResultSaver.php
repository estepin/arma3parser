<?php
use Geekstart\Arma3parserPack\ArmaObjectRepository;
use Geekstart\Arma3parserPack\ArmaObject;

class ResultSaver
{
    protected $pathToResult;
    protected $pathToResultImg;

    protected $paaConverter;
    protected $repository;

    function __construct($pathToResult, ArmaObjectRepository $repository)
    {
        $this->pathToResult = $pathToResult;
        $this->pathToResultImg = $pathToResult.'/img';
        $this->paaConverter = new PaaConverter();
        $this->repository = $repository;
    }

    /**
     * @param ArmaObject[] $armaObjects
     */

    function convertAndSave(array $armaObjects) : ConvertSaveReport
    {
        $this->clearResultDir();

        $report = new ConvertSaveReport();

        $countClassesFound = 0;

        foreach ($armaObjects as $k => $armaObject) {
            $countClassesFound++;
            $reportItem = new ConvertSaveReportItem();
            $reportItem->setClassName($armaObject->getClassName());

            $this->savePicture($armaObject, $report, $reportItem);

            $this->saveEditorPreview($armaObject, $report);

            $report->addResultItem($armaObject);
            $report->addReportItem($reportItem);
            $armaObjects[$k] = $armaObject;
        }

        $this->repository->saveAll($armaObjects);
        $report->setCountClassesSaved($countClassesFound);

        return $report;
    }

    protected function saveEditorPreview(ArmaObject $armaObject, ConvertSaveReport $report)
    {
        $editorPreviewPath = $armaObject->getEditorPreview();

        if($editorPreviewPath) {
            $editorPreviewPath = toRuntimePath($editorPreviewPath);

            if(file_exists($editorPreviewPath)) {
                $pathInfo = pathinfo($editorPreviewPath);
                if(!copy($editorPreviewPath, $outputPath = $this->getOutputPath($editorPreviewPath, $pathInfo['extension']))) {
                    throw new Exception();
                }
                $armaObject->setEditorPreview($this->getRelativeRepositoryPath($outputPath));
            } else {

                $countFileNotFound = $report->getCountConvertFileNotFound();
                $report->setCountConvertFileNotFound($countFileNotFound);
            }
        }
    }

    protected function savePicture(ArmaObject $armaObject, ConvertSaveReport $report, ConvertSaveReportItem $reportItem)
    {
        $picturePath = $armaObject->getPicturePath();
        $picturePath = $picturePath ? toRuntimePath($picturePath) : false;
        if($picturePath) {
            if(file_exists($picturePath)) {

                try {
                    $outputPath = $this->convertPaa($picturePath);
                    $armaObject->setPicturePath($this->getRelativeRepositoryPath($outputPath));

                    $reportItem->setStatus(ConvertSaveReportItem::STATUS_SUCCESS);
                    $countConvertFileSuccess = $report->getCountConvertFileSuccess();
                    $report->setCountConvertFileSuccess($countConvertFileSuccess+1);
                } catch (ImageConvertException $exception) {

                    $reportItem->setStatus(ConvertSaveReportItem::STATUS_WARNING);
                    $reportItem->setError(ConvertSaveReportItem::ERROR_FILE_CONVERT);
                    $reportItem->setErrorDescription(
                        'File '.$armaObject->getPicturePath().' convert error. '.$exception->getMessage()
                    );
                    $countConvertFileError = $report->getCountConvertFileError();
                    $report->setCountConvertFileError($countConvertFileError + 1);
                }

            } else {
                $reportItem->setStatus(ConvertSaveReportItem::STATUS_WARNING);
                $reportItem->setError(ConvertSaveReportItem::ERROR_FILE_NOT_FOUND);
                $reportItem->setErrorDescription('File '.$armaObject->getPicturePath().' not found');
                $countFileNotFound = $report->getCountConvertFileNotFound();
                $report->setCountConvertFileNotFound($countFileNotFound + 1);
            }
        } else {
            $reportItem->setStatus(ConvertSaveReportItem::STATUS_SUCCESS);
        }
    }

    protected function clearResultDir()
    {
        if(file_exists($this->pathToResult)) {
            delete_directory($this->pathToResult);
        }

        $this->repository->clearRepository();

        mkdir($this->pathToResult);
        mkdir($this->pathToResultImg);
    }

    protected function getOutputPath($picturePath, $extension = 'png') : string
    {
        return $this->pathToResultImg.'/'.md5($picturePath).'.'.$extension;
    }

    protected function convertPaa($picturePath)
    {
        $outputPath = $this->getOutputPath($picturePath);
        return $this->paaConverter->convert($picturePath, $outputPath);
    }

    protected function getRelativeRepositoryPath($path)
    {
        $res = str_replace($this->pathToResult, '', $path);
        if(substr($res, 0, 1) == '/' || substr($res, 0, 1) == '\\') {
            $res = substr($res, 1);
        }

        return $res;
    }

}