<?php


class ArmaScreenFinder
{
    protected $pathToScreenshots;

    function __construct($pathToScreenshots)
    {
        $this->pathToScreenshots = $pathToScreenshots;
    }

    /**
     * @param $className
     * @return array;
     */
    function findByClass($className)
    {
        $filePath = pathConcat($this->pathToScreenshots, $className.'.png');
        if(file_exists($filePath)) {
            if(is_file($filePath)) {
                return [$filePath];
            }
        }

        return [];
    }
}