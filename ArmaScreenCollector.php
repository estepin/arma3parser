<?php
use Geekstart\Arma3parserPack\ArmaObjectRepository;

class ArmaScreenCollector
{
    protected $pathToScreenshots;
    protected $pathToResult;
    protected $repository;
    protected $finder;
    protected $saveOnlyWithScreenshot;

    function __construct($pathToScreenshots, $pathToResult, ArmaObjectRepository $repository, $saveOnlyWithScreenshot = false)
    {
        $this->saveOnlyWithScreenshot = $saveOnlyWithScreenshot;
        $this->pathToScreenshots = $pathToScreenshots;
        $this->pathToResult = $pathToResult;
        $this->repository = $repository;
        $this->finder = new ArmaScreenFinder($pathToScreenshots);
        $this->converter = new PngConverter();
    }

    function run()
    {
        $objects = $this->repository->getAll();

        foreach ($objects as $k => $object) {
            $screenPaths = $this->finder->findByClass($object->getClassName());
            if($screenPaths) {
                $screenPaths = $this->convert($screenPaths);
                $object->setScreenshots($screenPaths);

                $objects[$k] = $object;
            } else {
                if($this->saveOnlyWithScreenshot) {
                    unset($objects[$k]);
                }
            }
        }

        $this->repository->saveAll($objects);
    }

    protected function convert($screenPaths) : array
    {
        $result = [];
        if(!file_exists(pathConcat($this->pathToResult, '/screenshots'))) {
           if(!mkdir(pathConcat($this->pathToResult, '/screenshots'))) {
               throw new Exception();
           }
        }
        foreach ($screenPaths as $screenPath) {
            $pathInfo = pathinfo($screenPath);
            $relativePath = pathConcat('screenshots/', $pathInfo['filename'].'.jpg');
            $relativePath = mb_strtolower($relativePath);
            $outFilename = pathConcat($this->pathToResult, $relativePath);
            $this->converter->convertToJpg($screenPath, $outFilename);
            $result[] = $relativePath;
        }

        return $result;
    }
}