<?php
include_once "boot.php";

$objects = Container::getArmaObjectRepository();
$screenFinder = new ArmaScreenFinder($CONF['pathToScreenshots']);

foreach($objects->getAll() as $armaObject) {
    $typeTree = $armaObject->getBaseTypeTree();
    if($armaObject->getScope() != 0 && $armaObject->getModel() && in_array('House', $typeTree)) {
        if(!$screenshots = $screenFinder->findByClass($armaObject->getClassName())) {
            if($armaObject->getBaseType() !== 'StaticShip') {
                $resClasses[] = $armaObject->getClassName();
            }
        }
    }
}

$content = file_get_contents('template.sqf');
$classesString = '"'.implode('","', $resClasses).'"';
$content = str_replace('#classes#', $classesString, $content);
$content = file_put_contents('template_result.sqf', $content);

echo "Count classes: ".count($resClasses).PHP_EOL;