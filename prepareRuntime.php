<?php
include_once "boot.php";
/**
 * @var array $CONF
 */
echo "Are you sure you want to do this?  Type 'yes' to continue: ";
if(!cliConfirm()) {
    echo "ABORTING".PHP_EOL.PHP_EOL;
    exit(1);
}

delete_directory('runtime');
mkdir('runtime');
mkdir('runtime/xmls');
mkdir('runtime/hpp');

foreach ($CONF['paths'] as $path) {
    $path = $CONF['pathToArma3'].DS.$path;
    if(!file_exists($path)) {
        throw new \Exception('Path not found '.$path);
    }
    $paths[] = $path;
}

foreach ($paths as $path) {
    $scandir = scandir($path);
    foreach ($scandir as $itemPath) {
        $fileinfo = pathinfo($itemPath);
        if(isset($fileinfo['extension']) && $fileinfo['extension'] == 'pbo') {
            $filenames[] = $path.DS.$itemPath;
        }
    }
}
$collector = new CfgCollector();

foreach($filenames as $filename) {
    if(preg_match('|.*\.pbo$|', $filename)) {
        $out = exec('BankRev\BankRev.exe -f runtime "'.$filename.'"');
        preg_match('|^Created (.*$)|is', $out, $matches);
        $unpboDir = $matches[1];
        $txtUnpboFile = 'runtime/'.pathinfo($filename)['filename'].'.txt';
        $forXmlName = pathinfo($filename)['filename'];


        $files = glob_tree_search($unpboDir, 'config.bin');

        foreach($files as $filepath) {
            $filepath = $unpboDir.'/'.$filepath;
            $filepath = str_replace('/', '\\', $filepath);

            $newfilename = "runtime\\xmls\\".$forXmlName.'+'.uniqid().".xml";

            exec(
                sprintf("CfgConvert\CfgConvert.exe -xml -dst %s %s ", $newfilename, $filepath)
            );

            //delete_directory($unpboDir);
            if(file_exists($txtUnpboFile)) {
                unlink($txtUnpboFile);
            }
        }

        $hppFiles = [];
        $hppFiles = glob_tree_search($unpboDir, '*.hpp');
        $hppFiles = array_merge($hppFiles, glob_tree_search($unpboDir, '*.cpp'));

        foreach($hppFiles as $filepath) {
            $filepath = $unpboDir.'/'.$filepath;
            $filepath = str_replace('/', '\\', $filepath);

            $newfilename = "runtime\\hpp\\".$forXmlName.'+'.uniqid().".hpp";

            $e = file_exists($filepath);
            copy($filepath, $newfilename);
        }
    }
}

