<?php
use Geekstart\Arma3parserPack\ArmaObject;

class ConvertSaveReport
{
    protected $reportItems;
    protected $countClassesSaved;
    protected $countConvertFileSuccess;
    protected $countConvertFileNotFound;
    protected $countConvertFileError;
    protected $resultItems;

    /**
     * @return ConvertSaveReportItem[] mixed
     */
    public function getReportItems()
    {
        return $this->reportItems;
    }

    /**
     * @param mixed $reportItems
     */
    public function setReportItems($reportItems): void
    {
        $this->reportItems = $reportItems;
    }

    function addReportItem(ConvertSaveReportItem $item)
    {
        $this->reportItems[] = $item;
    }

    /**
     * @return mixed
     */
    public function getCountClassesSaved()
    {
        return $this->countClassesSaved;
    }

    /**
     * @param mixed $countClassesSaved
     */
    public function setCountClassesSaved($countClassesSaved): void
    {
        $this->countClassesSaved = $countClassesSaved;
    }

    /**
     * @return mixed
     */
    public function getCountConvertFileSuccess()
    {
        return $this->countConvertFileSuccess;
    }

    /**
     * @param mixed $countConvertFileSuccess
     */
    public function setCountConvertFileSuccess($countConvertFileSuccess): void
    {
        $this->countConvertFileSuccess = $countConvertFileSuccess;
    }


    /**
     * @return mixed
     */
    public function getCountConvertFileNotFound()
    {
        return $this->countConvertFileNotFound;
    }

    /**
     * @param mixed $countConvertFileNotFound
     */
    public function setCountConvertFileNotFound($countConvertFileNotFound): void
    {
        $this->countConvertFileNotFound = $countConvertFileNotFound;
    }

    /**
     * @return mixed
     */
    public function getResultItems()
    {
        return $this->resultItems;
    }

    /**
     * @param mixed $resultItems
     */
    public function setResultItems($resultItems): void
    {
        $this->resultItems = $resultItems;
    }

    function addResultItem(ArmaObject $reportItem)
    {
        $this->resultItems[] = $reportItem;
    }

    /**
     * @return mixed
     */
    public function getCountConvertFileError()
    {
        return $this->countConvertFileError;
    }

    /**
     * @param mixed $countConvertFileError
     */
    public function setCountConvertFileError($countConvertFileError): void
    {
        $this->countConvertFileError = $countConvertFileError;
    }


}