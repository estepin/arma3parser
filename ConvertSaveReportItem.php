<?php


class ConvertSaveReportItem
{
    const STATUS_SUCCESS = 'success';
    const STATUS_WARNING = 'warning';
    const STATUS_ERROR = 'error';

    const ERROR_FILE_NOT_FOUND = 'file_not_found';
    const ERROR_FILE_CONVERT = 'file_not_found';

    protected $className;
    protected $status;
    protected $error;
    protected $errorDescription;

    /**
     * @return mixed
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @param mixed $className
     */
    public function setClassName($className): void
    {
        $this->className = $className;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error): void
    {
        $this->error = $error;
    }

    /**
     * @return mixed
     */
    public function getErrorDescription()
    {
        return $this->errorDescription;
    }

    /**
     * @param mixed $errorDescription
     */
    public function setErrorDescription($errorDescription): void
    {
        $this->errorDescription = $errorDescription;
    }


}