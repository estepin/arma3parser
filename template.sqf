_vehicles = [#classes#];

private _camera = "camera" camCreate (getMarkerPos "marker_1");
showCinemaBorder false;
_camera cameraEffect ["internal", "BACK"];
_camera camPrepareTarget (getMarkerPos "marker_1");
_camera camCommitPrepared 0;

[_vehicles, _camera] spawn {

    _camera = _this select 1;
    {
        _veh = _x createVehicle getMarkerPos "marker_1";
        _className = _x;

        if(isNull _veh) then {

        } else {
            _bbr = boundingBoxReal _veh;
            _sizeVeh = _bbr select 2;
            _camera camSetTarget _veh;
            if(_sizeVeh > 1) then {
                _camera camPrepareRelPos [_sizeVeh, _sizeVeh, (_sizeVeh / 1.7)];
            } else {
                _camera camPrepareRelPos [1, 1, 1];
            };
            
            _camera camCommitPrepared 0;
            _kindOf = '';
            if(_veh isKindOf 'AllVehicles') then {
                _kindOf = 'AllVehicles';
            };

            systemChat format ['SIZE %1 | CLASS %2 | KIND_OF %3', _sizeVeh, _className, _kindOf];

            sleep 2;
            screenshot format ['%1.png', _x];
            if(_veh isKindOf "Bag_Base") then {
                deleteVehicle objectParent _veh;
            } else {
                deleteVehicle _veh;
            };

            
            sleep 1;
        };
    } forEach (_this select 0);
};