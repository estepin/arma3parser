<?php

class HppParser
{
    function parse($path)
    {
        $content = $this->getContent($path);
        $content = $this->cutComments($content);
        $classContents = $this->findClassContents($content);

        $data = [];
        if(!$classContents) {
            return $data;
        }

        $groupClasses = ['CfgVehicles', 'CfgWeapons'];

        foreach ($classContents as $k => $classContent) {
            $classData = $this->parseClassContent($classContent);
            if(in_array($classData['className'], $groupClasses)) {
                unset($classContents[$k]);
                $classContents = array_merge($classContents, $this->findClassContents($classData['classBody']));
            }
        }


        foreach ($classContents as $classContent) {
            if($d = $this->parseUnitClass($classContent)) {
                $data[] = $d;
            }
        }

        return $data;
    }

    protected function cutComments($content)
    {
        $content = preg_replace('|//.*?\r\n|is', "\r\n", $content);
        $content = preg_replace('|/\*.*?\*/|is', '', $content);

        return $content;
    }

    protected function findClassContents($content)
    {
        $regexp = '/\s*class [\w\s_:]*? \{ (?>(?R) | ([\s\w\[\]+=]*? ({.*?} | [^{}]*);))* \s*\};/ixs';

        $result = preg_match_all($regexp, $content, $m);
        if($result) {
            return $m[0];
        } else {
            return [];
        }
    }

    protected function parseUnitClass($content)
    {
        $classData = $this->parseClassContent($content);

        if($classData['classBody']) {

            return [
                'name' => $classData['className'],
                'base' => $classData['classBaseName'],
                'compatibleItems' => false,
                'magazines' => false,
                'weapons' => false,

                'picture' => false,
                'generalMacro' => false,
                'side' => false,
                'faction' => false,
                'editorPreview' => false,
                'model' => false,
                'scope' => false,

                'fuelCapacity' => false,
                'maxSpeed' => false,
                'slingLoadMaxCargoMass' => false,
                'maximumLoad' => false,
                'armor' => false,

                'transportMaxMagazines' => false,
                'transportMaxWeapons' => false,
                'transportMaxBackpacks' => false,

                'weaponSlotsInfo' => false
            ];
        }
    }

    protected function parseCompatibleItems($content)
    {
        $subClassesContent = $this->findClassContents($content);
        if($subClassesContent) {
            foreach ($subClassesContent as $subClassContent) {
                $subClassData = $this->parseClassContent($subClassContent);
                if($subClassData['className'] == 'compatibleItems') {
                    $data = $this->parseClassProperties($subClassData['classBody']);
                    return array_keys($data);
                }
            }
        }

        return false;
    }

    protected function parseClassProperties($content)
    {
        $data = [];
        $lines = explode(';', $content);
        foreach ($lines as $line) {
            if(!$line) {
                continue;
            }

            $ex = explode('+=', trim($line));
            if(count($ex) != 2) {
                $ex = explode('=', trim($line));
            }
            $name = trim($ex[0]);
            $value = trim($ex[1]);

            if(isset($data[$name])) {
                throw new Exception();
            }
            $data[$name] = $value;
        }

        return $data;
    }

    protected function parseClassContent($content)
    {
        $content = trim($content);
        preg_match('/class\s([a-z0-9\-_]+)\s*(?::\s*([a-z0-9\-_]+)\s*)?{(.*)};/isx', $content, $classNameMatch);

        return [
            'className' => trim($classNameMatch[1]),
            'classBaseName' => isset($classNameMatch[2]) ? trim($classNameMatch[2]) : false,
            'classBody' => isset($classNameMatch[3]) ? trim($classNameMatch[3]) : false
        ];
    }

    protected function getContent($path)
    {
        return file_get_contents($path);
    }
}