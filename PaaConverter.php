<?php


class PaaConverter
{
    function convert($inputPath, $outputPath)
    {
        if(!file_exists($inputPath)) {
            return false;
        }
        $res = exec(sprintf('Pal2Pace\Pal2Pace.exe -size=256 %s %s', $inputPath, $outputPath));
        if($res != sprintf('%s->%s', $inputPath, $outputPath)) {
            throw new ImageConvertException($res);
        }

        if(!file_exists($outputPath)) {
            throw new Exception();
        }
        return $outputPath;
    }
}