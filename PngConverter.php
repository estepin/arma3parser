<?php


class PngConverter
{
    function convertToJpg($pathToImagePng, $outputPath)
    {
        $image = imagecreatefrompng($pathToImagePng);
        $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
        imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
        imagealphablending($bg, TRUE);
        imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
        imagedestroy($image);
        $quality = 50;
        if(!imagejpeg($bg, $outputPath, $quality)) {
            imagedestroy($bg);
            throw new Exception("Can't save jpeg file to ".$outputPath);
        }


        return $outputPath;
    }

}