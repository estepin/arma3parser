TexView 2

=========

    (c) 2011-2017 Bohemia Interactive a.s.

    All rights reserved.

    See http://community.bistudio.com/wiki/TexView_2 for more details of usage

    Please note that we provide this tool as it is and it's not currently under development (yet).