<?php


class CfgUnitParser
{
    protected $lastPath;
    protected $openTagsMatches;
    protected $closeTagsMatches;

    function parse($pathToConfig)
    {
        $this->openTagsMatches = null;
        $this->closeTagsMatches = null;

        $this->lastPath = $pathToConfig;
        $data = [];
        $content = $this->getContent($pathToConfig);

        $unitNames = array_unique($this->parseUnitNames($content));


        foreach ($unitNames as $unitName) {
            $data = array_merge($data, $this->parseUnit($unitName, $content));
        }

        return $data;
    }

    protected function parseUnit($unitName, $content)
    {
        preg_match_all('|<('.$unitName.') base="([a-zA-Z0-9_\-]*?)">(.*?)</'.$unitName.'>|is', $content, $weaponDataAll, PREG_OFFSET_CAPTURE);

        $result = [];
        foreach ($weaponDataAll as $key => $w) {
            if(!isset($weaponDataAll[1][$key][0]) || !$weaponDataAll[1][$key][0]) {
                continue;
            }
            $startPos = $weaponDataAll[0][$key][1];
            if(!$this->checkBlock($startPos, $content)) {
                continue;
            }

            $base = trim($weaponDataAll[2][$key][0]);
            $weaponDataContent = $weaponDataAll[3][$key][0];


            if($weaponDataContent) {

                $result[] = [
                    'name' => $unitName,
                    'base' => $base,

                    'magazines' => $this->parseArrayItemsUnitBlock($weaponDataContent, 'magazines'),
                    'weapons' => $this->parseArrayItemsUnitBlock($weaponDataContent, 'weapons'),

                    'picture' => $this->parseSimpleTag($weaponDataContent, 'picture'),
                    'generalMacro' => $this->parseSimpleTag($weaponDataContent, '_generalMacro'),
                    'side' => $this->parseSimpleTag($weaponDataContent, 'side'),
                    'faction' => $this->parseSimpleTag($weaponDataContent, 'faction'),
                    'editorPreview' => $this->parseSimpleTag($weaponDataContent, 'editorPreview'),
                    'model' => $this->parseSimpleTag($weaponDataContent, 'model'),
                    'scope' => $this->parseSimpleTag($weaponDataContent, 'scope', true),

                    'fuelCapacity' => $this->parseSimpleTag($weaponDataContent, 'fuelCapacity'),
                    'maxSpeed' => $this->parseSimpleTag($weaponDataContent, 'maxSpeed'),
                    'slingLoadMaxCargoMass' => $this->parseSimpleTag($weaponDataContent, 'slingLoadMaxCargoMass'),
                    'maximumLoad' => $this->parseSimpleTag($weaponDataContent, 'maximumLoad'),
                    'armor' => $this->parseSimpleTag($weaponDataContent, 'armor'),

                    'transportMaxMagazines' => $this->parseSimpleTag($weaponDataContent, 'transportMaxMagazines'),
                    'transportMaxWeapons' => $this->parseSimpleTag($weaponDataContent, 'transportMaxWeapons'),
                    'transportMaxBackpacks' => $this->parseSimpleTag($weaponDataContent, 'transportMaxBackpacks'),

                    'weaponSlotsInfo' => $this->parseWeaponSlotsInfo($weaponDataContent),
                    'compatibleItems' => $this->parseCompatibleItems($weaponDataContent)
                ];
            } else {
                $result[] = [
                    'name' => $unitName,
                    'base' => $base
                ];
            }
        }

        return $result;
    }

    protected function checkBlock($startPos, $content)
    {
        $openTags = $this->getParentOpenTags($content, $startPos);
        $acceptableBaseTags = ['CfgWeapons', 'CfgAmmo', 'CfgVehicles', 'CfgMagazines'];
        if(count($openTags) == 1) {
            if(in_array(array_shift($openTags), $acceptableBaseTags) === false) {
                return false;
            }
        } else if(count($openTags) > 1) {
            return false;
        }

        return true;
    }

    protected function getParentOpenTags($content, $tagPosition)
    {
        if($this->openTagsMatches === null) {
            preg_match_all('|<[a-zA-Z0-9_\-]*(?:\s*[\W]*)*?>|is', $content, $openTagsMatches, PREG_OFFSET_CAPTURE);

            foreach ($openTagsMatches[0] as $tag) {
                preg_match('|<([a-zA-Z0-9_\-]*)|is', $tag[0], $tagName);

                if(preg_match('|\/>$|is', $tag[0])) {
                    continue;
                }

                $this->openTagsMatches[] = [
                    'tagName' => $tagName[1],
                    'pos' => $tag[1]
                ];
            }
        }


        if($this->closeTagsMatches === null) {
            preg_match_all('|</[a-zA-Z0-9_\-]*?>|is', $content, $closeTagsMatches, PREG_OFFSET_CAPTURE);
            foreach ($closeTagsMatches[0] as $tag) {
                preg_match('|</([a-zA-Z0-9_\-]*)>|is', $tag[0], $tagName);
                $this->closeTagsMatches[] = [
                    'tagName' => $tagName[1],
                    'pos' => $tag[1]
                ];
            }
        }


        foreach ($this->openTagsMatches as $tag) {
            if($tag['pos'] < $tagPosition) {
                $currentOpenTags[] = $tag['tagName'];
            }
        }

        foreach ($this->closeTagsMatches as $tag) {
            if($tag['pos'] < $tagPosition) {
                $currentCloseTags[] = $tag['tagName'];
            }
        }

        $result = array_diff($currentOpenTags, $currentCloseTags);

        return $result;
    }

    protected function parseWeaponSlotsInfo($content)
    {
        $s = preg_match('|<WeaponSlotsInfo>(.*?)</WeaponSlotsInfo>|is', $content, $slotsInfoContent);
        if(!$s) {
            $s = preg_match('|<WeaponSlotsInfo base=".*?">(.*?)</WeaponSlotsInfo>|is', $content, $slotsInfoContent);
        }
        if($s) {
            $result = [];
            $slotsInfoContent = $slotsInfoContent[1];
            if(preg_match_all('|<([a-zA-Z0-9_\-]*?) base="([a-zA-Z0-9_\-]*?)">|is', $slotsInfoContent, $matches)) {
                foreach ($matches[1] as $k => $match) {
                    $name = $match;
                    $base = $matches[2][$k];
                    $compatibleItems = [];

                    if(preg_match(sprintf('|<%s base="%s">(.*?)</%s>|is', $name, $base, $name), $slotsInfoContent, $slotContent)) {
                        $slotContent = $slotContent[1];
                        if(preg_match('|<compatibleItems(\stype="array"){0,1}>(.*?)</compatibleItems>|is', $slotContent, $compatibleItemsContent)) {
                            $compatibleItemsContent = $compatibleItemsContent[1];
                            if(preg_match_all('|<item>([a-zA-Z0-9_\-]*?)</item>|is', $compatibleItemsContent, $items)) {
                                $compatibleItems = $items[1];
                            } elseif (preg_match_all('|<([a-zA-Z0-9_\-]*?)>[0-9]*?</[a-zA-Z0-9_\-]*?>|', $slotContent, $items)) {
                                $compatibleItems = $items[1];
                            }
                        }
                    }

                    $result[] = [
                        'name' => $name,
                        'base' => $base,
                        'compatibleItems' => $compatibleItems
                    ];
                }
            }

            return $result;
        } else {
            return null;
        }
    }

    protected function parseSimpleTag($content, $tagname, $caseSensitive = false)
    {
        $mod = 'is';
        if($caseSensitive) {
            $mod = 's';
        }

        if(preg_match('|<'.$tagname.'>(.*?)</'.$tagname.'>|'.$mod, $content, $matches)) {
            if(isset($matches[1]) && strlen($matches[1])) {
                return trim($matches[1]);
            }
        }

        return null;
    }


    protected function parseArrayItemsUnitBlock($content, $tagName)
    {
        if(preg_match_all('|<'.$tagName.' type="array">(.*?)</'.$tagName.'>|is', $content, $matches)) {
            if($matches[1]) {
                $res = [];
                foreach ($matches[1] as $matchItemsContent) {
                    if(strlen($matchItemsContent)) {
                        $res = array_merge($res, $this->parseItems($matchItemsContent));
                    }
                }
                return $res;
            }
        }
    }

    protected function parseCompatibleItems($content)
    {
        $content = preg_replace('|<WeaponSlotsInfo>(.*?)</WeaponSlotsInfo>|is', '', $content);
        $content = preg_replace('|<WeaponSlotsInfo base=".*?">(.*?)</WeaponSlotsInfo>|is', '', $content);

        if(!$compatibleItems = $this->parseArrayItemsUnitBlock($content, 'compatibleItems')) {
            if(preg_match('|<compatibleItems>(.*?)</compatibleItems>|is', $content, $matches)) {
                if(preg_match_all('|<([a-zA-Z0-9_\-]*?)>.*?</[a-zA-Z0-9_\-]*?>|is', $matches[1], $matches)) {
                    return $matches[1];
                }
            } elseif(preg_match('|<compatibleItems base="(.*?)">(.*?)</compatibleItems>|is', $content, $matches)) {
                if($matches[1] != 'compatibleItems') {
                    throw new \Exception();
                }

                if(preg_match_all('|<([a-zA-Z0-9_\-]*?)>.*?</[a-zA-Z0-9_\-]*?>|is', $matches[2], $matches)) {
                    return $matches[1];
                }
            }

            return [];
        } else {
            return $compatibleItems;
        }
    }

    protected function parseItems($itemsContent)
    {
        $result = [];
        preg_match_all('|<item>(.*?)</item>|is', $itemsContent, $matches);
        if(isset($matches[1])) {
            foreach ($matches[1] as $match) {
                $result[] = trim($match);
            }
        }

        return $result;
    }


    protected function parseUnitNames($content)
    {
        preg_match_all('|<(.*?[^>]) base=".*?">|i', $content, $classNamesContent);
        if(isset($classNamesContent[1])) {
            return $classNamesContent[1];
        }
        return [];
    }

    protected function getContent($path)
    {
        return file_get_contents($path);
    }
}