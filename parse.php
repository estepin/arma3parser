<?php
include_once "boot.php";

/**
 * @var ArmaObjectRepository $repository
 */
$startTime = time();
$paths = glob('runtime/xmls/*', GLOB_BRACE);
$paths = array_merge($paths, glob('runtime/hpp/*', GLOB_BRACE));
//$paths = array_slice($paths, 0, 10);

$arma3Parser = Container::getArma3Parser();
$arma3Parser->setPaths($paths);
$report = $arma3Parser->run();

foreach ($report->getConvertAndSaveReport()->getReportItems() as $item) {
    if($item->getStatus() == ConvertSaveReportItem::STATUS_WARNING) {
        echo 'WARNING '.$item->getErrorDescription().PHP_EOL;
    }
}
$endTime = time();
echo PHP_EOL;

echo "Classes saved: ".$report->getConvertAndSaveReport()->getCountClassesSaved().PHP_EOL;
echo "Pictures saved: ".$report->getConvertAndSaveReport()->getCountConvertFileSuccess().PHP_EOL;
echo "Pictures convert error: ".$report->getConvertAndSaveReport()->getCountConvertFileError().PHP_EOL;
echo "Pictures not found: ".$report->getConvertAndSaveReport()->getCountConvertFileNotFound().PHP_EOL;
echo "Runtime: ".($endTime - $startTime)." seconds".PHP_EOL.PHP_EOL;