<?php

use \Geekstart\Arma3parserPack\ArmaObjectRepository;

class Container
{
    static function getArma3Parser() : Arma3Parser
    {
        global $CONF;
        $resultSaver = new ResultSaver($CONF['pathResultDir'], self::getArmaObjectRepository());
        return new Arma3Parser($resultSaver);
    }

    static function getArmaObjectRepository() : ArmaObjectRepository
    {
        global $CONF;
        return new ArmaObjectRepository($CONF['pathResultDir']);
    }

    static function getArmaScreenCollector($saveOnlyWithScreenshot = false) : ArmaScreenCollector
    {
        global $CONF;
        return new ArmaScreenCollector($CONF['pathToScreenshots'], $CONF['pathResultDir'], self::getArmaObjectRepository(), $saveOnlyWithScreenshot);
    }
}