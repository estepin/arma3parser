<?php


function glob_tree_search($path, $pattern, $_base_path = null)
{
    if (is_null($_base_path)) {
        $_base_path = '';
    } else {
        $_base_path .= basename($path) . '/';
    }

    $out = array();
    foreach (glob($path . '/' . $pattern, GLOB_BRACE) as $file) {
        $out[] = $_base_path . basename($file);
    }

    foreach (glob($path . '/*', GLOB_ONLYDIR) as $file) {
        $out = array_merge($out, glob_tree_search($file, $pattern, $_base_path));
    }

    return $out;
}

function delete_directory($dirname)
{
    $dir_handle = false;
    if (is_dir($dirname))
        $dir_handle = opendir($dirname);
    if (!$dir_handle)
        return false;
    while ($file = readdir($dir_handle)) {
        if ($file != "." && $file != "..") {
            if (!is_dir($dirname . "/" . $file))
                unlink($dirname . "/" . $file);
            else
                delete_directory($dirname . '/' . $file);
        }
    }
    closedir($dir_handle);
    rmdir($dirname);
    return true;
}

function toRuntimePath($pathInConf)
{
    global $CONF;
    $prePath = ['pictureRepair', 'pictureHeal', 'pictureExplosive', 'pictureLogic', 'o_installation'];
    $pathInConf = str_replace('\\', '/', $pathInConf);

    if(in_array($pathInConf, $prePath)) {
        return false;
    }

    if(preg_match('|^#\(argb|is', $pathInConf)) {
        return false;
    }

    foreach ($CONF['replacePaths'] as $replacePath) {
        $e = '|'.$replacePath.'|is';

        if(preg_match($e, $pathInConf)) {
            $pathInConf = preg_replace($e, '', $pathInConf);
            break;
        }
    }

    $pathInfo = pathinfo($pathInConf);
    if(!isset($pathInfo['extension']) || !$pathInfo['extension']) {
        $pathInConf = $pathInConf.'.paa';
    }

    return 'runtime/'.$pathInConf;
}

function cliConfirm()
{
    $handle = fopen ("php://stdin","r");
    $line = fgets($handle);
    if(trim($line) != 'yes'){
        return false;
    }
    fclose($handle);
    return true;
}

function pathConcat($path1, $path2)
{
    $endSlash = substr($path1, strlen($path1)-1, 1);
    if($endSlash != '/' && $endSlash != '\\') {
        $path1 .= '/';
    }

    $beginSlash = substr($path2, 0, 1);
    if($beginSlash == '/' || $beginSlash == '\\') {
        $path2 = substr($path2, 1);
    }

    return $path1.$path2;
}