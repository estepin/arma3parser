<?php
use Geekstart\Arma3parserPack\ArmaObject;

class CfgCollector
{
    protected $parser;

    function __construct()
    {
        $this->xmlParser = new CfgUnitParser();
        $this->hppParser = new HppParser();
    }

    function parse($paths)
    {
        $data = [];
        foreach ($paths as $path) {
            $parser = $this->getParser($path);
            if($res = $parser->parse($path)) {
                foreach ($res as  $dataItem) {
                    $className = $dataItem['name'];
                    if(isset($data[$className])) {
                        $data[$className] = $this->mergeSameArray($data[$className], $dataItem);
                    } else {
                        $data[$className] = $dataItem;
                    }
                }
            }
        }

        $recursiveFields = [
            'picture', 'magazines', 'generalMacro', 'side', 'faction', 'editorPreview',
            'model', 'scope', 'fuelCapacity', 'maxSpeed', 'slingLoadMaxCargoMass', 'maximumLoad', 'armor',
            'weapons', 'transportMaxMagazines', 'transportMaxWeapons', 'transportMaxBackpacks', 'weaponSlotsInfo'
        ];

        $result = [];
        foreach ($data as $k => $item) {

            foreach ($recursiveFields as $fieldName) {
                $data[$k][$fieldName] = $this->recursiveFindData($item['name'], $fieldName, $data);
            }

            $data[$k]['baseTypeTree'] = $this->recursiveCollect($item['name'], $data);
        }

        foreach ($data as $k => $item) {
            $data[$k]['compatibleItems'] = $this->collectCompatibleItems($item, $data);
            $result[] = $this->arrayToObject($data[$k]);
        }

        return $result;
    }

    protected function getParser($path)
    {
        $ex = explode('/', $path);
        if($ex[1] == 'xmls') {
            return $this->xmlParser;
        } elseif ($ex[1] == 'hpp') {
            return $this->hppParser;
        } else {
            throw new \Exception("Can't detect parser");
        }
    }

    protected function mergeSameArray($arr1, $arr2)
    {
        foreach ($arr1 as $key => $value)
        {
            if($arr1[$key] === null && $arr2[$key] !== null) {
                $arr1[$key] = $arr2[$key];
            }
        }

        return $arr1;
    }

    protected function arrayToObject($data) : ArmaObject
    {
        $object = new ArmaObject();
        $object->setClassName($data['name']);
        $object->setBaseClassName($data['base']);
        $object->setGeneralMacro($data['generalMacro']);
        $object->setBaseTypeTree($data['baseTypeTree']);
        $object->setScope($data['scope']);

        $object->setFuelCapacity($data['fuelCapacity']);
        $object->setMaxSpeed($data['maxSpeed']);
        $object->setSlingLoadMaxCargoMass($data['slingLoadMaxCargoMass']);
        $object->setMaximumLoad($data['maximumLoad']);
        $object->setArmor($data['armor']);
        $object->setWeapons($data['weapons']);

        $object->setTransportMaxMagazines($data['transportMaxMagazines']);
        $object->setTransportMaxWeapons($data['transportMaxWeapons']);
        $object->setTransportMaxBackpacks($data['transportMaxBackpacks']);

        $object->setCompatibleItems($data['compatibleItems']);

        if($data['magazines']) {
            $object->setMagazines($data['magazines']);
        }

        if($data['picture']) {
            $object->setPicturePath($data['picture']);
        }

        if(strlen($data['side'])) {
            $object->setSide($data['side']);
        }

        if($data['faction']) {
            $object->setFaction($data['faction']);
        }

        if($data['editorPreview']) {
            $object->setEditorPreview($data['editorPreview']);
        }

        if($data['model']) {
            $object->setModel($data['model']);
        }

        return $object;
    }


    protected function recursiveFindData($name, $key, $data, $depth = 0)
    {
        if($depth > 20) {
            return null;
        }

        if(isset($data[$name])) {
            $item = $data[$name];
            if((is_array($item[$key]) && !$item[$key]) || (!is_array($item[$key]) && !strlen($item[$key]))) {
                if($item['base'] == $name) {
                    return null;
                }
                $depth++;
                return $this->recursiveFindData($item['base'], $key, $data, $depth);
            } else {
                return $item[$key];
            }
        }

        return null;
    }

    protected function recursiveCollect($name, $data, $result = [], $depth = 0)
    {
        if($depth > 100) {
            return $result;
        }


        if(isset($data[$name])) {
            $d = $data[$name];
            $result[] = $d['base'];
            if($d['base'] == $d['name']) {
                return $result;
            }
            $depth++;
            return $this->recursiveCollect($d['base'], $data, $result, $depth);
        }

        return $result;
    }

    protected function collectCompatibleItems($item, $data)
    {
        if($item['weaponSlotsInfo']) {
            $compatibleItems = [];
            foreach ($item['weaponSlotsInfo'] as $weaponSlot) {
                if(!$weaponSlot['compatibleItems']) {
                    if($d = $this->recursiveFindData($weaponSlot['base'], 'compatibleItems', $data)) {
                        $compatibleItems = array_merge($compatibleItems, $d);
                    }

                } else {
                    $compatibleItems = array_merge(
                        $compatibleItems,
                        $weaponSlot['compatibleItems']
                    );
                }
            }

            return array_unique($compatibleItems);
        }
    }
}